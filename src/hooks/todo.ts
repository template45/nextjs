import useTodoStore from '../stores/users';

const useTodo = () => {
  const items = useTodoStore((state) => state.items);
  const addItem = useTodoStore((state) => state.addItem);
  const removeItem = useTodoStore((state) => state.removeItem);

  return {
    items,
    addItem,
    removeItem
  }
}
export default useTodo;