import { AxiosResponse } from 'axios';
import { GET } from './api';

interface Todo {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}
export const getTodos = (): Promise<AxiosResponse<Todo[]>> => {
  return GET('/todos');
}