'use client';

import React from 'react';
import Style from './style';

interface ButtonProps {
  children: React.ReactNode;
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  secondary?: boolean;
  danger?: boolean;
  /**
   * How large should the button be?
   */
  size?: 'small' | 'medium' | 'large';
  /**
   * Optional click handler
   */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  /**
   * Button type, default button
   */
  type?: 'button' | 'submit' | 'reset' | undefined;
  className?: string;
  disabled?: boolean;
  loading?: boolean;
  style?: object;
}

/**
 * Primary UI component for user interaction
 */

const Button = ({
  children,
  primary = false,
  secondary = false,
  danger = false,
  disabled = false,
  size = 'medium',
  type = 'button',
  className = '',
  loading = false,
  ...props
}: ButtonProps) => {
  let mode = primary ? 'primary' : '';
  if (secondary) {
    mode = 'secondary';
  }
  if (danger) {
    mode = 'danger';
  }

  return (
    <Style
      {...props}
      disabled={disabled}
      type={type}
      className={[className, size, mode, loading ? 'loading' : ''].join(' ')}
      onClick={(e) => {
        if (!disabled && !loading && props.onClick) {
          props.onClick(e);
        }
      }}
    >
      {children}
    </Style>
  );
};

export default Button;
