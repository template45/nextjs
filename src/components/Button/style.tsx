import styled from '@emotion/styled';

export default styled.button`
  label: button;
  padding: 6px 24px;
  margin: 0;
  font-size: 16px;
  display: inline-block;
  font-family: 'Kanit', sans-serif;
  transition: all 0.4s ease-in-out;
  cursor: pointer;
  height: 48px;
  font-weight: 600;
  background: var(--white);
  color: var(--gray-900);
  border: 1px solid var(--gray-100);
  text-align: center;
  :disabled {
    cursor: default;
    opacity: 0.7;
  }
  &.loading {
   display: flex !important;
   justify-content: center;
   align-items: center;
  }
  &.large {
    height: 56px;
  }
  &.small {
    height: 32px;
  }
  &.primary {
    background: var(--primary-500);
    color: var(--white);
    border: 1px solid var(--primary-500);
  }
  &.secondary {
    border: 1px solid var(--primary-100);
    background: var(--primary-100);
    color: var(--primary-500);
  }
  &.danger {
    border: 1px solid var(--error-100);
    background: var(--error-100);
    color: var(--error-500);
  }
`;
