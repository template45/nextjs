'use client';

import { getTodos } from '../services/todo';
import React from 'react';
import useTodo from '../hooks/todo';

const HomePage = () => {
  const { addItem, items, removeItem } = useTodo();
  const textInput: React.RefObject<HTMLInputElement> = React.useRef(null);

  const loadCustomers = async () => {
    try {
      const res = await getTodos();
      console.log(
        'res',
        res.data.map((obj) => {
          return obj.title;
        })
      );
    } catch (err) {
      console.error('err', err);
    }
  };

  React.useEffect(() => {
    loadCustomers();
  }, []);

  const handleAdd = () => {
    // const input:  = textInput;
    if (textInput.current) {
      addItem(textInput.current.value);
      textInput.current.value = '';
      textInput.current.focus();
    }
  };

  return (
    <div>
      <h1>Home</h1>
      <input
        type="text"
        ref={textInput}
      />
      <button onClick={handleAdd}>Add</button>
      <ul>
        {items.map((value, index) => {
          return (
            <li key={value}>
              {value}
              <button onClick={() => removeItem(index)}>Remove</button>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default HomePage;
