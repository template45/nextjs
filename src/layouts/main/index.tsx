import type { Metadata } from 'next';

import LayoutContent from './Content';

import '../../styles/style.scss';

export const metadata: Metadata = {
  title: 'Demo Application',
  description: '...',
};

const LayoutMain = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <html lang="en">
      <LayoutContent>{children}</LayoutContent>
    </html>
  );
};

export default LayoutMain;
