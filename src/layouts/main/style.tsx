import styled from '@emotion/styled';

export default styled.body`
  padding-top: 60px;
  header {
    background-color: var(--primary-700);
    padding: 0 10px;
    height: 60px;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    z-index: 99;
    display: flex;
    align-items: center;
    justify-content: center;
    nav ul {
      display: flex;
      align-items: center;
      justify-content: center;
      li {
        list-style-type: none;
        a {
          font-weight: normal;
          font-size: 18px;
          text-decoration: none;
          padding: 6px 10px;
          color: var(--white);
          position: relative;
          &::after {
            z-index: 2;
            content: '';
            opacity: 0;
            visibility: hidden;
            position: absolute;
            bottom: 0;
            left: 6px;
            right: 6px;
            height: 1px;
            background: var(--white);
            transform: translateY(6px);
            transition: all 0.4s ease-in-out;
          }
        }
        &.active > a {
          &::after {
            opacity: 1;
            visibility: visible;
            transform: translateY(0px);
          }
        }
      }
    }
  }
`;
