'use client';

import Link from 'next/link';

import Style from './style';
import { usePathname } from 'next/navigation';

const Content = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  const pathname = usePathname();
  const menus = [
    {
      link: '/',
      label: 'Home',
      validate: /^\/$/,
    },
    {
      link: '/about',
      label: 'About',
      validate: /^\/about/,
    },
  ];

  return (
    <Style>
      <header>
        <nav>
          <ul>
            {menus.map((menu) => {
              return (
                <li
                  key={menu.link}
                  className={menu.validate.test(pathname) ? 'active' : ''}
                >
                  <Link href={menu.link}>{menu.label}</Link>
                </li>
              );
            })}
          </ul>
        </nav>
      </header>
      {children}
    </Style>
  );
};

export default Content;
