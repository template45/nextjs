import '../../styles/style.scss';

import type { Metadata } from 'next';
export const metadata: Metadata = {
  title: 'Admin',
  description: '...',
};
const LayoutAdmin = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <html lang="en">
      <h1>Admin</h1>
      <body>{children}</body>
    </html>
  );
};

export default LayoutAdmin;
