import { create } from "zustand";

interface TodoState {
  items: string[];
  addItem: (item: string) => void,
  removeItem: (index: number) => void,
  clearItems: () => void,
}

const useTodoStore = create<TodoState>()((set) => ({
  items: [],
  addItem: (item) => set((state) => ({
    ...state,
    items: [...state.items, item]
  })),
  removeItem: (index) => set((state) => ({
    ...state,
    items: [...state.items.slice(0, index),
    ...state.items.slice(index + 1)]
  })),
  clearItems: () => set((state) => ({ ...state, items: [] })),
}))

// const items = useTodoStore((state) => state.items);
// const addItem = useTodoStore((state) => state.addItem);
// const removeItem = useTodoStore((state) => state.removeItem);
// const clearItems = useTodoStore((state) => state.clearItems);

// export { items, addItem, removeItem, clearItems };
export default useTodoStore;